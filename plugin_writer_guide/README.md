Gstreamer Plugins
===================
A plugin is essentially a loadable block of code, usually called 
a shared object file or a dynamically linked library.

## **Revicing the Basics** 
----------
## Elements
Elements are at the core of GStreamer. In the context of plugin 
development, an element is an object derived from the GstElement 
class.ust writing a new element is not entirely enough, however: 
You will need to encapsulate your element in a plugin to enable 
GStreamer to use it.  A single plugin may contain the 
implementation of several elements, or just a single one.

## Pads
Pads are used to negotiate links and data flow between elements in 
GStreamer. A pad can be viewed as a “place” or “port” on an element 
where links may be made with other elements, and through which data 
can flow to or from those elements. Pads have specific data handling 
capabilities: A pad can restrict the type of data that flows through 
it. Links are only allowed between two pads when the allowed data 
types of the two pads are compatible.

## GstMiniObject
All streams of data in GStreamer are chopped up into chunks that 
are passed from a source pad on one element to a sink pad on 
another element. GstMiniObject is the structure used to hold these
chunks of data.

GstMiniObject contains the following important types:

- An exact type indicating what type of data (event, buffer, ...) 
this GstMiniObject is.

- A reference count indicating the number of elements currently 
holding a reference to the miniobject. When the reference count 
falls to zero, the miniobject will be disposed, and its memory will 
be freed in some sense

During transport there is two types of GstMiniObjects defined:
    
- **buffers (content)** : a buffer contains a chunk of some sort 
of audio or video data that flows from one element to another

    Buffers also contain metadata describing the buffer's contents. 
    Some of the important types of metadata are:
    - Pointers to one or more GstMemory objects. GstMemory objects 
    are refcounted objects that encapsulate a region of memory.
    - A timestamp indicating the preferred display timestamp of the 
    content in the buffer.
- **events (control)** : Events contain information on the state of 
the stream flowing between the two linked pads.
    > Events will only be sent if the element explicitly supports them, 
    else the core will (try to) handle the events automatically.

    *Events are used to indicate, for example, a media type, the end 
    of a media stream or that the cache should be flushed.*

    Events may contain several of the following items:

    - A subtype indicating the type of the contained event.
    - The other contents of the event depend on the specific event type.

### Buffer Allocation
Buffers are able to store chunks of memory of several different types.
> The most generic type of buffer contains memory allocated by malloc().
 Such buffers, although convenient, are not always very fast, since 
 data often needs to be specifically copied into the buffer.

The buffer freeing code automatically determines the correct method of
freeing the underlying memory. Downstream elements that receive these
kinds of buffers do not need to do anything special to handle or
unreference it.

 *more detailed data [here](https://gstreamer.freedesktop.org/documentation/plugin-development/introduction/basics.html?gi-language=c#buffer-allocation)*

Many sink elements have accelerated methods for copying data to 
hardware, or have direct access to hardware. It is common for 
these elements to be able to create a GstBufferPool or GstAllocator 
for their upstream peers. One such example is ximagesink.
    
> ximagesink creates buffers that contain XImages. Thus, when an 
upstream peer copies data into the buffer, it is copying directly into 
the XImage, enabling ximagesink to draw the image directly to the 
screen instead of having to copy data into an XImage first.

*See more in [Memory allocation](https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/allocation.html?gi-language=c)*

## Media Types and Properties

 >List of Defined Media Types can be found [here](https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/media-types.html?gi-language=c#list-of-defined-media-types)

**Table of example types**

![tableOfExampleTypes](images/listTable.png)
