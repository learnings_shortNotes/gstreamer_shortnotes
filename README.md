Gstreamer
==========

>Tutorial: [Gstreamer](https://gstreamer.freedesktop.org/documentation/tutorials/basic/index.html?gi-language=c), 

## GST dataTypes
- GstElement *pipeline;
- GstBus *bus;
- GstMessage *msg;
- GstStateChangeReturn ret;
## GST Commands
- Initialise gstreamer
    ```
    gst_init (&argc, &argv);
    ```
    - Initializes all internal structures
    - Checks what plug-ins are available
    - Executes any command-line option intended for GStreamer
- Create a simple pipeline with ease
    ```
    pipeline = gst_parse_launch("playbin uri=https://Web url/to/media", NULL);
    ```
    *playbin is a special element which acts as a source and as a sink, and is a whole pipeline.*
    > if you wish to connect a local file source: uri=file:///

- Set state of an element
    ```
    gst_element_set_state (pipeline, GST_STATE_PLAYING);

    ```
- Wait until and error occurs:
    ```
    bus = gst_element_get_bus (pipeline);
    msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
    ```
- Free resources:
    - Free elements:
        ```
        gst_object_unref(elementName);
        ```
    - Free Messages:
        ```
        gst_message_unref(msg);
        ```
- Create GST Elements:
    ```
    source = gst_element_factory_make ("videotestsrc", "source");
    sink = gst_element_factory_make ("autovideosink", "sink");
    ```
- Create empty Pipeline
    ```
    pipeline = gst_pipeline_new ("name_of_pipeline");
    ```
- Build the pipeline
    ```
    gst_bin_add_many (GST_BIN (pipeline), source, sink, NULL);
    ```
- Check for link between elements
    ```
    if (gst_element_link (source, sink) != TRUE) {
        g_printerr ("Elements could not be linked.\n");
        gst_object_unref (pipeline);
        return -1;
    }
    ```
- Check if state changed
    ```
    ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        g_printerr ("Unable to set the pipeline to the playing state.\n");
        gst_object_unref (pipeline);
        return -1;
    }
    ```
